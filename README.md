#Getting set up

In order to develop an add-on for the Atlassian Server products you'll:

* The Atlassian SDK
* A modern Java installation (basically - you'll need to match what Atlassian's software runs on - see [Jira supported platforms](https://confluence.atlassian.com/adminjiraserver075/supported-platforms-935390828.html) for an example of these - more than likely if you've got Java 1.8 installed - you're safe :)
* (Optional) an IDE. You don't need it - you can code in vim (yes I just went there) but a real IDE is preferable.


##Installing the Atlassian sdk

In order to develop an add-on for the Atlassian Server products you'll need to install the Atlassian SDK. The SDK is basically just a wrapper around a bunch of maven goals (for more about Maven - please see [https://maven.apache.org/what-is-maven.html]().

NOTE: The Atlassian SDK bundles maven so you don't need to worry this.

To install the SDK - head on over to the below urls and install the SDK from there.

* [Windows](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project/install-the-atlassian-sdk-on-a-windows-system)
* [Mac or Linux](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project/install-the-atlassian-sdk-on-a-linux-or-mac-system)

###Verify your SDK installation

Once you've installed the SDK, you'll need to verify that it's installed correctly. Simply open a command line prompt and execute `atlas-version`. The output should show something like this(paths and version numbers may differ :) ):

![atlas-version.png](atlas-version.png)

If you don't get this - check your paths and/or follow any error messages that you get.

###Really verify your SDK installation

Once you've gotten the SDK to be installed, you'll want to verify that maven etc works properly - execute `atlas-run-standalone --product jira --version 7.5.1`. The output should look like something like:

![](atlas-run-start.png)

After a while it should show things like:

![](atlas-run-standalone-progress.png)

If you don't see the things like (or get errors)...:
> Downloading: https://maven.atlassian.com/repository/public/com/atlassian/jira/plugins/jira-plugin-test-resources/7.5.1/jira-plugin-test-resources-7.5.1.zip

.. you'll want to check your internet connection and make sure that your computer has access to https://maven.atlassian.com.

This command will take a while to run and eventually you'll get 
> [INFO] [talledLocalContainer] Tomcat 8.x started on port [2990]
[INFO] jira started successfully in 166s at http://Daniels-MacBook-Pro-3.local:2990/jira

In your terminal window. At that point you should be able to access that url in a browser window and access Jira. For all SDK instances that are brought up - the username/password will be admin/admin (Bitbucket Server usually adds a user/user user as well).

### Congrats! You have an SDK installation

Press CTRL-C and feel free to experiment with the various atlas-* commands that are available to you. For more information about the commands - take a look at [https://developer.atlassian.com/docs/developer-tools/working-with-the-sdk/command-reference/atlas-run-standalone]().

# Get your IDE setup (optional)

If you're going to use an IDE - you'll want to set up Maven within the IDE. Go back to the `atlas-version` command - look for the `ATLAS Maven Home` output. This is the home directory of Maven. Inside of this directory there will be a conf/settings.xml - you'll want the absolute path to this.

![](maven-config.png)

For example for my installation, this was:
>ATLAS Maven Home: /Applications/Atlassian/atlassian-plugin-sdk-6.2.2/apache-maven-3.2.1

Which resulted in a settings.xml file at /Applications/Atlassian/atlassian-plugin-sdk-6.2.2/apache-maven-3.2.1/conf/settings.xml.

##Head into the IDE

In your IDE, you'll want to configure the Maven Preferences. In IntelliJ IDEA Community Edition this will be under 'Preferences' > Maven. Your milage may vary. Once there though you'll want to change the 'Maven home directory' to match the output of the ATLAS Maven Home and the User settings file to be settings.xml in the conf file.

![](idea-config.png)


# Congrats you're done! 

Head on over to the next tutorial and we'll start coding!
